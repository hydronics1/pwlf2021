//
// Created by James Moore on 1/21/21.
//

#include "meteor.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 60;

void meteorRain(int max_loops) {

    FastLED.clear();

    for (int loops = 0; loops <= max_loops; loops++) {

        int sat = 255;
        int meteorTrailDecay = random8(100, 255);
        int meteorSize = 5;
        int num_frames = CENTER;
        int draw_pos;

        byte e_hue = random8(255);
        byte s_hue = e_hue + 127;


        for (int frame = 0; frame < num_frames; frame++) {
            updateTimers();
            random16_add_entropy(random());

            // map the frame number into 0,1 space
            float p = map((float)frame, 0.0f, (float) num_frames, 0, 1);
//            if (p > 0.992) break; // cut off the long tail of the exponetial curve

            // apply easing
            float eased_p = QuinticEaseOut(p);

            // map back to position
            int e_pix = map(eased_p, 0, 1.0, EAST_START, -meteorSize);
            int s_pix = map(eased_p, 0, 1.0, SOUTH_START, SOUTH_END + meteorSize);

            // Create a random fading trail
            for (int j = 0; j <= NUM_LEDS; j++) {
                if (random8(10) > 7) {
                    leds[j].fadeToBlackBy(meteorTrailDecay);
                }
            }

            for (int j = meteorSize; j >= 0; j--) {
                draw_pos = e_pix - j;
                if (draw_pos <= EAST_START && draw_pos >= 0) {
                    leds[draw_pos].setHSV(e_hue, sat, 255);;
                }
            }

            for (int j = 0; j < meteorSize; j++) {
                draw_pos = s_pix - j;
                if (draw_pos <= SOUTH_END && draw_pos >= SOUTH_START) {
                    leds[draw_pos].setHSV(s_hue, sat, 255);;
                }
            }

            FastLED.show();
            FastLED.delay(1000 / FRAMES_PER_SECOND);

        }
    }

    fadeOut(0, NUM_LEDS);
}
