//
// Created by James Moore on 1/26/21.
//

#ifndef PWLF2021_COUNTDOWN_H
#define PWLF2021_COUNTDOWN_H

void countdown(int duration);

#endif //PWLF2021_COUNTDOWN_H
