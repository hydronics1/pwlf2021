//
// Created by James Moore on 1/27/21.
//

#include "planets.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 200;
const float length = NUM_LEDS * 2.0f;
const int max_width = 7;

float wrap_pos(float pos) {
    if (pos > length) pos -= length;
    else if (pos < 0) pos += length;

    return pos;
}

void drawBar(float pos, const CHSV &color, int width) {
    for (int pix = 0; pix < width; pix++) {
        float adj_pos = wrap_pos(pos + pix);
        if ((adj_pos >= 0) && (adj_pos <= NUM_LEDS)) {
            leds[lround(pos + pix)] = color;
        }
    }
}

float pos_map(float pos) {

    pos = wrap_pos(pos);

    // adjust the position forward so the center ot the easing lands near the strip CENTER
    float adj = CENTER - 3.0;

    float f_result;

    if (pos <= NUM_LEDS) {
        float p = map(pos, 0.0, (float) NUM_LEDS, 0, 1);
        float eased_p = QuadraticEaseIn(p);
        f_result = map(eased_p, 0.0, 1.0, 0, NUM_LEDS);
    } else {
        float p = map(pos, (float) NUM_LEDS + 1, length, 0, 1);
        float eased_p = QuadraticEaseOut(p);
        f_result = map(eased_p, 0.0, 1.0, NUM_LEDS + 1, length);
    }

    // adjust the new postion forward so the easiing is tied to CENTER
    float result = wrap_pos(f_result + adj);

//    Serial.printf("In Pos: %i\tP: %f\tEased P:%f\tf_result: %f\tresult: %i\n",
//                  pos, p, eased_p, f_result, result );
    return result;
}

int width_map(float pos) {
    pos = wrap_pos(pos);
    int width;
    int offset = max_width / 2;

    if (pos <= CENTER - offset) {
        width = map(pos, 0, CENTER - offset, 1, max_width);
    } else  if (pos >= CENTER + offset) {
        width = map(pos, CENTER + offset, NUM_LEDS, max_width, 1);
        if (width < 1) width = 1;
    } else {
        width = max_width;
    }

    return width;
}

void planets(int duration) {
    FastLED.clear();
    resetStopwatch();

    float p1_pos = 0.0;
    float p1_v = float_rand(2.0, 2.5);
    CHSV p1_color = {random8(255), 255, 255};

    float p2_pos = 0.0;
    float p2_v = float_rand(1.5, 2.0);
    CHSV p2_color = {0, 255, 255};
    p2_color.hue = p1_color.hue + 64;

    float p3_pos = 0.0;
    float p3_v = float_rand(1.0, 1.5);
    CHSV p3_color = {0, 255, 255};
    p3_color.hue = p2_color.hue + 64;

    float p4_pos = 0.0;
    float p4_v = float_rand(0.5, 1.0);
    CHSV p4_color = {0, 255, 255};
    p4_color.hue = p3_color.hue + 64;


    while (stopwatch < duration * 1000) {

        updateTimers();
        FastLED.clear();

        // draw sun
        leds[CENTER] = CRGB::Yellow;

        float eased_p1_pos = pos_map(p1_pos);
        float eased_p2_pos = pos_map(p2_pos);
        float eased_p3_pos = pos_map(p3_pos);
        float eased_p4_pos = pos_map(p4_pos);
//        Serial.printf("Red: %i\n", eased_red_pos);

        int p1_width = width_map(eased_p1_pos);
        int p2_width = width_map(eased_p2_pos);
        int p3_width = width_map(eased_p3_pos);
        int p4_width = width_map(eased_p4_pos);

        Serial.printf("pos: %f\teased: %f\tw: %i\n", p1_pos, eased_p1_pos, p1_width);

        drawBar(eased_p1_pos, p1_color, p1_width);
        drawBar(eased_p2_pos, p2_color, p2_width);
        drawBar(eased_p3_pos, p3_color, p3_width);
        drawBar(eased_p4_pos, p4_color, p4_width);

        p1_pos = wrap_pos(p1_pos + p1_v);
        p2_pos = wrap_pos(p2_pos + p2_v);
        p3_pos = wrap_pos(p3_pos + p3_v);
        p4_pos = wrap_pos(p4_pos + p4_v);

        FastLED.show();
        FastLED.delay(1000 / FRAMES_PER_SECOND);

    }

    fadeOut(0, NUM_LEDS);

}