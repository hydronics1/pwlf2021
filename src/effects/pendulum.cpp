//
// Created by James Moore on 1/24/21.
//

#include "pendulum.h"
#include "lib/shared.h"

const int FRAMES_PER_SECOND = 100;
const int pendulum_size = 3;
const float energy_loss = 0.90f;

CHSV e_color = { 0, 255, 255};
CHSV s_color = { 0, 255, 255};
CHSV c_color = { 255, 0, 255}; // White

float energy;

void draw_pendulums(int east_pos, int south_pos) {

    FastLED.clear();

    if (east_pos - 1 >= 0) leds[east_pos - 1] = e_color;
    if (east_pos >= 0) leds[east_pos] = e_color;
    if (east_pos + 1 >= 0) leds[east_pos + 1] = e_color;

    if (south_pos - 1 <= NUM_LEDS) leds[south_pos - 1] = s_color;
    if (south_pos <= NUM_LEDS) leds[south_pos] = s_color;
    if (south_pos + 1 <= NUM_LEDS) leds[south_pos + 1] = s_color;

    leds[CENTER] = c_color;

}

int swing_map(int i, int start, int end, bool in) {
    // map the east_pos into 0,1 space
    float p = map((float) i, (float) start, (float) end, 0, 1);
    float eased_p;

    // apply easing
    if (in) {
        eased_p = SineEaseIn(p);
    } else {
        eased_p = SineEaseOut(p);
    }

    // map back to east_pos
    double f_result = map(eased_p, 0.0, 1.0, (float) start, (float) end);
    int result;

    result = lround(f_result);

//    Serial.printf("I: %i\tStart: %i\tEnd: %i\tP: %f\tEased P: %f\tRaw Result: %f\tResult: %i\n", i, start, end, p, eased_p, f_result, result);

    return result;
}

void pendulum() {

    e_color.hue = random8(255);
    s_color.hue = e_color.hue + 127;

    int east_min_pos = CENTER - 2;
    int east_pos = east_min_pos;

    int south_min_pos = CENTER + 2;
    int south_pos = south_min_pos;

    int max_pos;

    draw_pendulums(east_pos, south_pos);
    leds[CENTER].setHSV(255, 0, 255);
    FastLED.show();
    FastLED.delay(1000);

    energy = 70.0f;
    int eased_east_pos, eased_south_pos;

    FastLED.clear();

    while (energy >= pendulum_size) {
        updateTimers();

        max_pos = CENTER - (int) energy;
        for (int i = east_min_pos; i >= max_pos; i--) {

            eased_east_pos = swing_map(i, east_min_pos, max_pos, false);
            draw_pendulums(eased_east_pos, south_pos);
            FastLED.show();
            FastLED.delay(1000 / FRAMES_PER_SECOND);
        }

        for (int i = max_pos; i <= east_min_pos; i++) {

            eased_east_pos = swing_map(i, max_pos, east_min_pos, true);
            draw_pendulums(eased_east_pos, south_pos);
            FastLED.show();
            FastLED.delay(1000 / FRAMES_PER_SECOND);

            if (eased_east_pos == east_min_pos) break; // Stop animation when its back to center
        }

        // transfer the energy to the other side (pick end position)
        energy = energy * energy_loss;
        if (energy < 3) break; // Exit early if the energy is too low
        c_color.hue = random8(255);
        c_color.saturation = 255; // allow the center to show a color


        max_pos = CENTER + (int) energy;

        // animate out and back
        for (int i = south_min_pos; i <= max_pos; i++) {

            eased_south_pos = swing_map(i, south_min_pos, max_pos, false);
            draw_pendulums(east_pos, eased_south_pos);
            FastLED.show();
            FastLED.delay(1000 / FRAMES_PER_SECOND);
        }

        for (int i = max_pos; i >= south_min_pos; i--) {

            eased_south_pos = swing_map(i, max_pos, south_min_pos, true);
            draw_pendulums(east_pos, eased_south_pos);
            FastLED.show();
            FastLED.delay(1000 / FRAMES_PER_SECOND);

            if (eased_south_pos == south_min_pos) break; // Stop animation when its back to center
        }

        // transfer energy to the east side
        energy = energy * energy_loss;
        c_color.hue = random8(255);

    }

    FastLED.delay(1000);
    fadeOut(CENTER - pendulum_size, CENTER + pendulum_size);

}

