//
// Source: https://gist.github.com/kriegsman/062e10f7f07ba8518af6
//

#include "small.h"
#include "lib/shared.h"

void confetti(int duration) {

    FastLED.clear();
    resetStopwatch();
    while (stopwatch < duration * 1000) {

        updateTimers();

        // random colored speckles that blink in and fade smoothly
        fadeToBlackBy(leds, NUM_LEDS, 10);
        int pos = random16(NUM_LEDS);
        leds[pos] += CHSV(random8(255), 255, 255);
        FastLED.show();
    }
    fadeOut(0, NUM_LEDS);

}

void juggle(int duration) {

    FastLED.clear();
    resetStopwatch();
    while (stopwatch < duration * 1000) {

        updateTimers();

        // eight colored dots, weaving in and out of sync with each other
        fadeToBlackBy( leds, NUM_LEDS, 20);
        byte dothue = 0;
        for( int i = 0; i < 8; i++) {
            leds[beatsin16( i+7, 0, NUM_LEDS-1 )] |= CHSV(dothue, 200, 255);
            dothue += 32;
        }
        FastLED.show();
    }
    fadeOut(0, NUM_LEDS);

}

