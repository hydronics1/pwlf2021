#include <effects/rgb.h>
#include <effects/planets.h>
#include <effects/small.h>
#include "lib/shared.h"
#include "effects/fire.h"
#include "effects/meteor.h"
#include "effects/pendulum.h"
#include "effects/snow.h"
#include "effects/countdown.h"
#include "effects/pacifica.h"

const int EAST_START = 72;
const int EAST_END = 0;
const int CENTER = 73;
const int SOUTH_START = 74;
const int SOUTH_END = 120;
const int NUM_LEDS = _NUM_LEDS;
const int DATA_PIN = 27;
const int BRIGHTNESS = 255;
int ct = 0;
int lt = 0;
int dt = 0;
int stopwatch = 0;

#define COLOR_ORDER GRB // Testing
//#define COLOR_ORDER RBG // Production

CRGB leds[NUM_LEDS];
uint8_t gHue = 0;

void setup() {
    Serial.begin(115200);
//    while (!Serial); // wait for serial connection before starting

    FastLED.delay(1000); // sanity delay
    FastLED.addLeds<WS2811, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS);
    FastLED.setBrightness(BRIGHTNESS);
    FastLED.clear();
    FastLED.show();

    trng_init();
    random16_set_seed(trng());

    ct = millis();
    lt = ct;
}

void loop() {
  // Turn the LED on, then pause
  leds[0] = CRGB::Red;
  FastLED.show();
  delay(500);
  // Now turn the LED off, then pause
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(500);
}
