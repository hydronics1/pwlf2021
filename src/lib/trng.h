//
// Created by James Moore on 1/24/21.
//

#ifndef PWLF2021_TRNG_H
#define PWLF2021_TRNG_H

#include <cstdint>


void trng_init();
uint32_t trng();

#endif //PWLF2021_TRNG_H
